<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="text" indent="no"/>

  <xsl:template match="/">
    <xsl:text>[</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template match="*:KeyFamilies">
    <xsl:apply-templates>
      <xsl:sort select="@id"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="*:KeyFamily">
    <xsl:text>{"name":"</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>","title":"</xsl:text>
    <xsl:value-of select="*:Name[1]"/>
    <xsl:text>"}</xsl:text>

    <xsl:if test="position() != last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
    <xsl:text>&#xa;</xsl:text>
  </xsl:template>

  <xsl:template match="text()"/>
</xsl:stylesheet>
