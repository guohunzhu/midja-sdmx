<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="text" indent="no"/>

  <xsl:template match="/">
    <xsl:text>{&#xa;</xsl:text>

    <xsl:for-each select="//*:Text">
      <xsl:apply-templates select="."/>
      <xsl:if test="position() != last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
      <xsl:text>&#xa;</xsl:text>
    </xsl:for-each>

    <xsl:text>}&#xa;</xsl:text>
  </xsl:template>

  <xsl:template match="*:Text">
    <xsl:text>"</xsl:text>
    <xsl:value-of select="../@id"/>
    <xsl:text>":"</xsl:text>
    <!--<xsl:value-of select="text()"/>-->
    <xsl:call-template name="escapeQuote"/>
    <xsl:text>"</xsl:text>
  </xsl:template>

  <xsl:template name="escapeQuote">
    <xsl:param name="pText" select="."/>

    <xsl:if test="string-length($pText) >0">
      <xsl:value-of select="substring-before(concat($pText, '&quot;'), '&quot;')"/>

      <xsl:if test="contains($pText, '&quot;')">
        <xsl:text>\"</xsl:text>
        <xsl:call-template name="escapeQuote">
          <xsl:with-param name="pText" select="substring-after($pText, '&quot;')"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template match="text()"/>
</xsl:stylesheet>
