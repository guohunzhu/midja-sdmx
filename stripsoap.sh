#!/bin/bash

for  f in responses/*.xml
do
  filename="${f##*/}"
  base="${filename%.[^.]*}"
  java -jar -Xmx2000m SaxonHE9-6-0-1J/saxon9he.jar -s:${f} -xsl:xsl/stripsoap.xsl > responsesstripped/${base}_stripped.xml
done