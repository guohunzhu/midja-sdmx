var datasets = require('./datasetstest.json');
var fs = require('fs');
var mustache = require('mustache');
var request = require('sync-request');


var datasetTemplate = fs.readFileSync("./requests/dsd_req_template.xml", "utf8");
var genericFullTemplate = fs.readFileSync("./requests/genericfull_req_template.xml", "utf8");


datasets.forEach(function(d){
  var name = d.name;
  console.log("downloading " + name);
  // get generic full data set
  var postData = mustache.render(genericFullTemplate, {dataset:d.name});

  var res = request('POST', "http://stat.abs.gov.au/sdmxws/sdmx.asmx", {
    headers: {
      "Accept": "text/plain",
      "Host": "stat.abs.gov.au",
      "Content-Type": "text/xml; charset=utf-8",
      "Content-Length": postData.length,
      "SOAPAction": "http://stats.oecd.org/OECDStatWS/SDMX/GetGenericData"
    },
    body: postData
  });
  fs.writeFile("./responses/genericfull_" + name + ".xml", res.body, function(err) {
    if(err) {
      console.log(err);
    } 
  });

});