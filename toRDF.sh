#!/bin/bash

for f in ../../responsesstripped/genericfull_*.xml
do
  filename="${f##*/}"
  base="${filename%.[^.]*}"
  base2="${base##genericfull_}"
  echo $base2
  saxonb-xslt -t -tree:linked -ext:on -s ../../responsesstripped/dsd_${base2}.xml -xsl generic.xsl xmlDocument=../../responsesstripped/dsd_${base2}.xml pathToGenericStructure=../../responsesstripped/dsd_${base2}.xml > ../../rdf/${base2}.dsd.rdf
  saxonb-xslt -t -tree:linked -ext:on -s ../../responsesstripped/${filename} -xsl generic.xsl xmlDocument=../../responses/${filename} pathToGenericStructure=../../responsesstripped/dsd_${base2}.xml > ../../rdf/${base2}.generic.rdf
done



