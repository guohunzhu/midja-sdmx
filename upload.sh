#!/bin/bash
for f in rdf/*_stripped.generic.rdf
do
  filename="${f##*/}"
  base="${filename%.[^.]*}"
  base2="${base%_stripped.generic}"
  echo $base2
  curl -X PUT -H "Content-Type:application/rdf+xml" -T ${f} localhost:8282/openrdf-sesame/repositories/midja-sdmx/rdf-graphs/service?graph=http%3A%2F%2Fmidja.org%2Fns%2Fdata%2F${base2}
  curl -X PUT -H "Content-Type:application/rdf+xml" -T rdf/${base2}_stripped.dsd.rdf localhost:8282/openrdf-sesame/repositories/midja-sdmx/rdf-graphs/service?graph=http%3A%2F%2Fmidja.org%2Fns%2Fdata%2F${base2}_DSD
done



