var datasets = require('./datasetstest.json');
var fs = require('fs');

var mustache = require('mustache');
var request = require('sync-request');


var metadataTemplate = fs.readFileSync("./requests/metadata_req_template.xml", "utf8");

datasets.forEach(function(d){
  var name = d.name;
  console.log("downloading " + name);
  // get metadata for dataset
  var postData = mustache.render(metadataTemplate, {dataset:d.name});

  var res = request('POST', "http://stat.abs.gov.au/sdmxws/sdmx.asmx", {
    headers: {
      "Accept": "text/plain",
      "Host": "stat.abs.gov.au",
      "Content-Type": "text/xml; charset=utf-8",
      "Content-Length": postData.length,
      "SOAPAction": "http://stats.oecd.org/OECDStatWS/SDMX/GetDatasetMetadata"
    },
    body: postData
  });
  fs.writeFile("./responses/metadata_" + name + ".xml", res.body, function(err) {
    if(err) {
      console.log(err);
    } 
  });

    /* 
    // switched to sync-request as ABS Stat service did not like multiple requests at the same time
    var unirest = require('unirest');
    unirest.post("http://stat.abs.gov.au/sdmxws/sdmx.asmx")
      .headers({
        "Accept": "text/plain",
        "Host": "stat.abs.gov.au",
        "Content-Type": "text/xml; charset=utf-8",
        "Content-Length": postData.length,
        "SOAPAction": "http://stats.oecd.org/OECDStatWS/SDMX/GetDataStructureDefinition"
      })
      .send(postData)
      .end(function(response) {
        fs.writeFile("./responses/dsd_" + name + ".xml", response.body, function(err) {
        if(err) {
          console.log(err);
        } 
      }); 
    });
*/

});