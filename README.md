

# Mapping ABS SDMX data to RDF for Midja Knowledge Base

This project was developed for [Midja](http://midja.org/) Integrated Knowledge Base on Aboriginal Housing by [UQ ITEE eResearch Group](http://www.itee.uq.edu.au/eresearch/). Midja is supported under the Australian Research Council's Discovery Indigenous Projects funding scheme (IN140100033).

## Download ABS data via SDMX

The following Node.js scripts have been created to retrieve data from SDMX.

 * _downloadMetadata.js_ - get the description of the fields for all datasets
 * _downloadDataSets.js_ - get the dataset description for all datasets
 * _downloadDataGeneric.js_ - get the full data for datasets

Install [Node.js](http://nodejs.org/) and then install the dependencies for the project using Node Package Manager

    $ cd midja-sdmx
    $ npm install

Copy some dataset JSON entries from the datasets.json file (list of all datasets on ABS SDMX) to datasetstest.json (trying to get all datasets usually fails - try in small batches). Then run using node e.g.:

    $ node downloadMetadata.js

After running the scripts, downloaded data will be saved as separate SDMX XML files in the responses directory.

The data will be wrapped in soap XML elements. To work with the linked-sdmx XSLT scripts, these will need to be removed. Run the stripsoap.sh script to remove all of the soap XML wrappers from the actual SDMX message content.

    $ ./stripsoap.sh

(This will transform all files from the responses directory saving the results to responsesstripped directory)

## Transform SDMX XML to RDF

The downloaded data can then be transformed from SDMX XML to RDF using XSLT using the linked-sdmx stylesheets.

Clone linked-sdmx project

    $ git clone https://github.com/csarven/linked-sdmx.git

Install dependencies for linked-sdmx (on ubuntu):

    $ sudo apt-get install libsaxonb-java
    $ sudo apt-get install raptor2-utils

To run the transformation:

    $ cp toRDF.sh linked-sdmx/scripts
    $ cd linked-sdmx/scripts
    $ ./toRDF.sh
    
The transformed files will now be in the rdf directory. 

## Running Sesame RDF Triplestore

Getting a local sesame server set up:

 * Install tomcat (at least version 6)
   * By default it will run on port 8080. To change this to another port, edit /etc/tomcat6/server.xml

 * Download Sesame from http://rdf4j.org/
   * Extract contents of distribution
   * Copy openrdf-sesame and openrdf-workbench wars to /var/lib/tomcat6/webapps

 * In /etc/tomcat6/catalina.properties add the following to customise the data directory (default location on ubuntu is /usr/share/tomcat6/.aduna)

    $ info.aduna.platform.appdata.basedir=/mnt/sesame/.aduna
 
   * See the [Sesame Manual](http://rdf4j.org/sesame/2.7/docs/users.docbook?view#chapter-datadir-config) for more

 * restart tomcat for changes to take effect

    $ sudo service tomcat6 restart

 * View the Sesame workbench in the Browser e.g. at localhost:8080/openrdf-workbench
 * Create a sesame repository by selecting 'New repository' from the menu and following the prompts. The easiest option is to select Native Java Store.
 * If you want to create a PostgreSQL RDF Store, create a database/role first:

    $ psql -U postgres
    create role sesame with login password 'demopassword';
    create database sesame_store with owner sesame;
 
* If you choose to store your data in PostgreSQL you will need to download the postgresql JDBC driver jar file from http://jdbc.postgresql.org/ and put it in /usr/share/tomcat6/lib

 * All of the generated RDF files can now be imported into Sesame.

Note: When working with millions of triples, it may be necessary to increase the memory available to avoid out of memory errors. Increase memory for heap, perm size etc via the JAVA_OPTS environment variable (e.g. add to /usr/share/tomcat6/bin/catalina.sh)

    JAVA_OPTS="$JAVA_OPTS -Xms1536m -Xmx1536m -XX:PermSize=256m"

## Uploading to Sesame

Change back to the root directory of the repository and run the upload script to upload any rdf files from the rdf directory to the sesame repository (this script assumes tomcat is running on localhost port 8282 - modify to suit your development environment e.g. 8080)

   $ cd ../..
   $ ./upload.sh


## SDMX requests

Request templates are in the requests directory.

Details on how to formulate different types of requests below:

### Retrieve list of all datasets
#### Raw XML 
`curl -0 -s 'http://stat.abs.gov.au/sdmxws/sdmx.asmx' -X POST -H 'Host: stat.abs.gov.au' -H 'Accept: text/plain' -H 'Content-Type: text/xml; charset=utf-8' -H 'SOAPAction: http://stats.oecd.org/OECDStatWS/SDMX/GetDataStructureDefinition' -d @datasets_req0.xml`

Notes for using curl with SDMX endpoint:

     -0 needed to force curl to not wait for 'Expect: 100-continue' or simply add
     -H 'Expect:' without -0


#### JSON 
Converts the XML response to a json array using XSLT. Run this command to refresh the contents of the datasets.json file.

`curl -0 -s 'http://stat.abs.gov.au/sdmxws/sdmx.asmx' -X POST -H 'Host: stat.abs.gov.au' -H 'Accept: text/plain' -H 'Content-Type: text/xml; charset=utf-8' -H 'SOAPAction: http://stats.oecd.org/OECDStatWS/SDMX/GetDataStructureDefinition' -d @datasets_req0.xml | java -jar saxon9he.jar -s:- -xsl:datasets_json.xsl`



### Retrieve metadata for a dataset

#### Raw XML
Change the common:ID of the dataset in the request XML to get a different dataset.

`curl -s -H 'Expect:' 'http://stat.abs.gov.au/sdmxws/sdmx.asmx' -X POST -H 'Host: stat.abs.gov.au' -H 'Accept: text/plain' -H 'Content-Type: text/xml; charset=utf-8' -H 'SOAPAction: http://stats.oecd.org/OECDStatWS/SDMX/GetDatasetMetadata' -d @metadata_ABS_CENSUS2011_B01_req0.xml`


#### JSON
As above, but run through XSLT to convert to JSON.

`curl -s -H 'Expect:' 'http://stat.abs.gov.au/sdmxws/sdmx.asmx' -X POST -H 'Host: stat.abs.gov.au' -H 'Accept: text/plain' -H 'Content-Type: text/xml; charset=utf-8' -H 'SOAPAction: http://stats.oecd.org/OECDStatWS/SDMX/GetDatasetMetadata' -d @metadata_ABS_CENSUS2011_B01_req0.xml | java -jar saxon9he.jar -s:- -xsl:metadata_json.xsl`



### Retrieve DataStructureDefinition

The DSD contains Dimensions/Concepts/CodeLists

#### Raw XML 
`curl -0 -s 'http://stat.abs.gov.au/sdmxws/sdmx.asmx' -X POST -H 'Host: stat.abs.gov.au' -H 'Accept: text/plain' -H 'Content-Type: text/xml; charset=utf-8' -H 'SOAPAction: http://stats.oecd.org/OECDStatWS/SDMX/GetDataStructureDefinition' -d @dsd_ABS_CENSUS2011_B01_req0.xml`




### Retrieve data

#### XML (generic) full dataset
The generic data response is quite verbose - responses will often be hundreds of megabytes in size.

`curl -s -H 'Expect:' 'http://stat.abs.gov.au/sdmxws/sdmx.asmx' -X POST -H 'Host: stat.abs.gov.au' -H 'Accept: text/plain' -H 'Content-Type: text/xml;charset=utf-8' -H 'SOAPAction: http://stats.oecd.org/OECDStatWS/SDMX/GetGenericData' -d @genericdata_fulldataset_req0.xml`

#### XML (generic:raw) with query
Use Query element in request XML to select only the dimensions required.

`curl -s -H 'Expect:' 'http://stat.abs.gov.au/sdmxws/sdmx.asmx' -X POST -H 'Host: stat.abs.gov.au' -H 'Accept: text/plain' -H 'Content-Type: text/xml; charset=utf-8' -H 'SOAPAction: http://stats.oecd.org/OECDStatWS/SDMX/GetGenericData' -d @genericdata_req0.xml`

#### XML (compact:raw) 
`curl -s -H 'Expect:' 'http://stat.abs.gov.au/sdmxws/sdmx.asmx' -X POST -H 'Host: stat.abs.gov.au' -H 'Accept: text/plain' -H 'Content-Type: text/xml; charset=utf-8' -H 'SOAPAction: http://stats.oecd.org/OECDStatWS/SDMX/GetCompactData' -d @compactdata_req0.xml`

Almost identical request as genericdata; 2 changes:

 1. GetCompactData instead of GetGenericData in SOAPAction header
 2. GetCompactData instead of GetGenericData as child element of soap:Body
     element in XML payload






